# Grupo Turing Website

Repositório contendo o website do Grupo Turing

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Install [Jekyll](https://jekyllrb.com/docs/installation/) :

```
gem install bundler jekyll
```

### Installing

* Install the dependencies

```
bundle install
```

* Run the website on localhost

```
jekyll serve
```

## Built With

* [Jekyll](https://jekyllrb.com/) - The web framework used

## Authors

* **Beatriz Passanezi**
* **Eduardo Netto**
* **Guilherme Marques**
* **Matheus Guinezi**
* **Saint Clair Bernardes**

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

Spectral by HTML5 UP
html5up.net | @n33co
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)

Icons:
* Font Awesome (fortawesome.github.com/Font-Awesome)

Other:
* jQuery (jquery.com)
* html5shiv.js (@afarkas @jdalton @jon_neal @rem)
* background-size polyfill (github.com/louisremi)
* Misc. Sass functions (@HugoGiraudel)
* Respond.js (j.mp/respondjs)
* Skel (skel.io)